isort:
	isort --recursive .

lint:
	gitlint && pylint no_zero_days && flake8 . && mypy .

check:
	make isort && make lint

run:
	python manage.py runserver

freeze:
	pip freeze > requirements.txt

migrations:
	python manage.py makemigrations

migrate:
	python manage.py migrate

makemessages:
	python manage.py makemessages --extension=py,html --ignore=venv --no-location

compilemessages:
	python manage.py compilemessages