from django.contrib import admin
from django.urls import path

from no_zero_days.views import ApplicationTemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', ApplicationTemplateView.as_view()),
]
