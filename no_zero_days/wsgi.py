import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'no_zero_days.settings')

application = get_wsgi_application()
