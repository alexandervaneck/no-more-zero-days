from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel


class Action(TimeStampedModel):
    description = models.CharField(
        max_length=140,
        help_text=_('Describe an action you can do in a short amount of time.')
    )
