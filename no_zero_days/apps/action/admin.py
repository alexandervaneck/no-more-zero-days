from django.contrib import admin

from no_zero_days.apps.action.models import Action

admin.site.register(Action)
