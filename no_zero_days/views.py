from django.views.generic import TemplateView


class ApplicationTemplateView(TemplateView):
    template_name = 'application.html'
